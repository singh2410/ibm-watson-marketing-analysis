#!/usr/bin/env python
# coding: utf-8

# # IBM Watson Marketing Analysis & Prediction
# #By- Aarush Kumar
# #Dated: July 13,2021

# In[1]:


from IPython.display import Image
Image(url='https://www.mobihealthnews.com/sites/default/files/IBM%20Watson.png')


# In[2]:


import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns               
get_ipython().run_line_magic('matplotlib', 'inline')
sns.set()
from subprocess import check_output


# In[3]:


def annot_plot(ax,w,h):                                    # function to add data to plot
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    for p in ax.patches:
         ax.annotate(f"{p.get_height() * 100 / df.shape[0]:.2f}%", (p.get_x() + p.get_width() / 2., p.get_height()),
         ha='center', va='center', fontsize=11, color='black', rotation=0, xytext=(0, 10),
         textcoords='offset points')             
def annot_plot_num(ax,w,h):                                    # function to add data to plot
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    for p in ax.patches:
        ax.annotate('{0:.1f}'.format(p.get_height()), (p.get_x()+w, p.get_height()+h))


# In[4]:


df = pd.read_csv('/home/aarush100616/Downloads/Projects/IBM Watson Marketing Data Analysis/WA_Fn-UseC_-Marketing-Customer-Value-Analysis.csv')


# In[5]:


df


# In[6]:


df.Response = df.Response.apply(lambda X : 0 if X == 'No' else 1)


# In[7]:


df


# ## Exploratory Data Analysis(EDA)

# In[8]:


df.info()


# In[9]:


df.isnull().sum()


# In[10]:


df.describe()


# ## Response Rate

# In[11]:


ax = sns.countplot('Response',data = df)
plt.ylabel('Total number of Response')
annot_plot(ax, 0.08,1)
plt.show()


# In[12]:


ax = sns.countplot('Response',hue = 'Gender' ,data = df)
plt.ylabel('Total number of Response')
annot_plot(ax, 0.08,1)
plt.show()


# In[13]:


plt.figure(figsize=(12,6))
ax = sns.countplot('Response', hue = df['Marital Status'], data = df)
annot_plot(ax,0.08,1)
plt.show()


# ## Response rate by renew offer

# In[14]:


plt.figure(figsize=(8,4))
ax = sns.countplot('Response',hue = 'Renew Offer Type' ,data = df)
plt.ylabel('Total number of Response')
annot_plot(ax, 0.08,1)
plt.show()


# ## Response rate by Education

# In[15]:


plt.figure(figsize=(8,4))
ax = sns.countplot('Response',hue = 'Education' ,data = df)
plt.ylabel('Total number of Response')
annot_plot(ax, 0.08,1)
plt.show()


# ## Response rate by Sales Channel

# In[17]:


plt.figure(figsize=(8,4))
ax = sns.countplot('Response',hue = 'Sales Channel' ,data = df)
plt.ylabel('Total number of Response')
annot_plot(ax, 0.08,1)
plt.show()


# ## Response rate by Total Claim Amount

# In[18]:


plt.figure(figsize=(12,6))
sns.boxplot(y = 'Total Claim Amount' , x = 'Response', data = df)
plt.ylabel('Total number of Response')
plt.show()


# ## Response rate by Income Distributions

# In[19]:


plt.figure(figsize=(12,6))
sns.boxplot(y = 'Income' , x = 'Response', data = df)
plt.show()


# ## Response rate by EmploymentStatus

# In[20]:


plt.figure(figsize=(10,6))
ax = sns.countplot('Response',hue = 'EmploymentStatus' ,data = df)
plt.ylabel('Total number of Response')
annot_plot(ax, 0.08,1)
plt.show()


# ## Response rate by Vehicle Class

# In[21]:


plt.figure(figsize=(10,6))
ax = sns.countplot('Response',hue = 'Vehicle Class' ,data = df)
plt.ylabel('Total number of Response')
annot_plot(ax, 0.08,1)
plt.show()


# ## Response rate by Policy

# In[22]:


plt.figure(figsize=(15,6))
ax = sns.countplot('Response',hue = 'Policy' ,data = df)
plt.ylabel('Total number of Response')
annot_plot(ax, 0.08,1)
plt.show()


# ## Regression Analysis with Continuous Variables Only

# In[23]:


import statsmodels.api as sm


# In[24]:


continous_var_df = df.select_dtypes(include=['int64','float'])
continous_var_df.nunique()


# In[25]:


continous_var_df.columns


# In[26]:


continous_var_reg = sm.Logit(continous_var_df['Response'], continous_var_df.drop('Response', axis = 1))
continous_var_reg.fit().summary()


# In[27]:


plt.figure(figsize=(10,6))
sns.heatmap(continous_var_df.corr(), annot = True)
plt.show()


# ## Regression Analysis with Categorical Variables

# In[28]:


categorical_df = df.select_dtypes(include='object')
cat_df = categorical_df.drop(['Customer','Effective To Date'], axis = 1)


# In[29]:


cat_df.nunique()


# ## Conversion of Categorical data

# In[30]:


cat_df.columns


# In[31]:


cols = ['State', 'Coverage', 'Education', 'EmploymentStatus', 'Gender',
       'Location Code', 'Marital Status', 'Policy Type', 'Policy',
       'Renew Offer Type', 'Sales Channel', 'Vehicle Class', 'Vehicle Size']


# In[32]:


from sklearn.preprocessing import LabelEncoder
lb = LabelEncoder()
for col in cat_df[cols]:
    cat_df[col] = lb.fit_transform(cat_df[col])


# In[33]:


cat_df.head()


# In[34]:


categorical_train = sm.Logit(continous_var_df.Response, cat_df)
categorical_train.fit().summary()


# ## Regression Analysis with bith Continous and Categorical Variables

# In[35]:


continous_var_df.reset_index(drop = True, inplace=True)
cat_df.reset_index(drop = True, inplace=True)


# In[36]:


all_data_df = pd.concat([continous_var_df,cat_df], axis = 1)


# In[37]:


all_data_df.head()


# In[38]:


total_train = sm.Logit(all_data_df.Response, all_data_df.drop(['Response'], axis = 1))
total_train.fit().summary()


# ## Regression Analysis with excluding Non-significant variables

# In[39]:


all_data_df.columns


# In[40]:


significant_cols = ['Customer Lifetime Value','Income','Monthly Premium Auto','Months Since Last Claim',
                    'Months Since Policy Inception','Number of Policies','Total Claim Amount','Marital Status',
                    'Renew Offer Type','Sales Channel','Vehicle Size']
trainData = sm.Logit(all_data_df.Response, all_data_df[significant_cols])
trainData.fit().summary()


# ## Classification

# In[41]:


y = all_data_df.Response
X = all_data_df.drop('Response', axis = 1)


# In[42]:


from sklearn.model_selection import train_test_split, cross_validate
X_train,  X_test, y_train, y_test = train_test_split(X,y, test_size = 0.3, random_state = 42)


# In[43]:


ax = sns.countplot(y_test)
annot_plot_num(ax,0.08,1)


# ## SVC Classification

# In[44]:


from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.svm import SVC
svc = SVC()
svc.fit(X_train, y_train)
svc_pred = svc.predict(X_test)
print(confusion_matrix(svc_pred,y_test))
print('accuracy_score:',accuracy_score(svc_pred, y_test))
print(classification_report(svc_pred, y_test))
cross_val_score_svc = cross_validate(svc, X_train, y_train,cv = 5)
print('Cross validation train_score',cross_val_score_svc['train_score'].mean())
print('Cross validation test_score',cross_val_score_svc['test_score'].mean())


# ## RandomForestClassifier

# In[45]:


from sklearn.ensemble import RandomForestClassifier
rfc = RandomForestClassifier()
rfc.fit(X_train, y_train)
rfc_pred = rfc.predict(X_test)
print(confusion_matrix(rfc_pred,y_test))
print('Accuracy score:',accuracy_score(rfc_pred, y_test))
print(classification_report(rfc_pred, y_test))
cross_val_score_rfc = cross_validate(rfc, X_train, y_train,cv = 5)
print('Cross validation train_score',cross_val_score_rfc['train_score'].mean())
print('Cross validation test_score',cross_val_score_rfc['test_score'].mean())


# ## Feature Importance

# In[46]:


feature_imp = rfc.feature_importances_.round(3)
ser_rank = pd.Series(feature_imp, index=X.columns).sort_values(ascending = False)
plt.figure(figsize=(12,7))
sns.barplot(x= ser_rank.values, y = ser_rank.index, palette='deep')
plt.xlabel('relative importance')
plt.show()

